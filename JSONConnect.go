package JSONConnect

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net/http"
	"path"
	"runtime"
	"strings"
	"time"

	Logging "gitlab.com/UrsusArcTech/logger"
)

var connectionError = make(map[string]bool)

func retrieveCallInfo() (packageName string, funcName string, line int, fileName string) {
	pc, file, line, _ := runtime.Caller(3)
	_, fileName = path.Split(file)
	parts := strings.Split(runtime.FuncForPC(pc).Name(), ".")
	pl := len(parts)

	funcName = parts[pl-1]

	if parts[pl-2][0] == '(' {
		funcName = parts[pl-2] + "." + funcName
		packageName = strings.Join(parts[0:pl-2], ".")
	} else {
		packageName = strings.Join(parts[0:pl-1], ".")
	}

	return packageName, funcName, line, fileName
}

func connErr(packageName string) {
	Logging.LogMessage("Retrying connection...")
	connectionError[packageName] = true
	time.Sleep(time.Second * 2)
}

func GetUnmarshalJSON[jsonStruct interface{}](updates *jsonStruct, url string, httpRequestType string) error {
	callerPackageName, _, _, _ := retrieveCallInfo()
	httpClient := http.Client{
		Timeout: time.Second * 4,
	}

	req, err := http.NewRequest(string(httpRequestType), url, nil)
	if err != nil {
		Logging.LogError(err.Error())
		connErr(callerPackageName)
		return err
	}

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		Logging.LogError(getErr.Error())
		connErr(callerPackageName)
		return getErr
	}

	if res.Body != nil {
		defer res.Body.Close()
	} else {
		connErr(callerPackageName)
		return errors.New("HTML body empty for: " + url)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		Logging.LogError(readErr.Error())
		connErr(callerPackageName)
		return readErr
	}

	jsonErr := json.Unmarshal(body, &updates)
	if jsonErr != nil {
		Logging.LogError(jsonErr.Error())
		connErr(callerPackageName)
		return err
	}

	if connectionError[callerPackageName] {
		Logging.LogMessage("Connection found!")
		connectionError[callerPackageName] = false
	}

	return nil
}

func GetXMLUnmarshal[xmlStruct interface{}](updates *xmlStruct, url string) error {
	callerPackageName, _, _, _ := retrieveCallInfo()
	httpClient := http.Client{
		Timeout: time.Second * 4,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		Logging.LogError(err.Error())
		connErr(callerPackageName)
		return err
	}

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		Logging.LogError(getErr.Error())
		connErr(callerPackageName)
		return getErr
	}

	if res.Body != nil {
		defer res.Body.Close()
	} else {
		connErr(callerPackageName)
		return errors.New("HTML body empty for: " + url)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		Logging.LogError(readErr.Error())
		connErr(callerPackageName)
		return readErr
	}

	jsonErr := xml.Unmarshal(body, &updates)
	if jsonErr != nil {
		Logging.LogError(jsonErr.Error())
		connErr(callerPackageName)
		return err
	}

	if connectionError[callerPackageName] {
		Logging.LogMessage("Connection found!")
		connectionError[callerPackageName] = false
	}

	return nil
}

func GetHTTPBody(url string) ([]byte, error) {
	callerPackageName, _, _, _ := retrieveCallInfo()
	httpClient := http.Client{
		Timeout: time.Second * 4,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		Logging.LogError(err.Error())
		connErr(callerPackageName)
		return nil, err
	}

	req.Header.Set("User-Agent", `Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.27 Safari/537.36`)

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		Logging.LogError(getErr.Error())
		connErr(callerPackageName)
		return nil, getErr
	}

	if res.Body != nil {
		defer res.Body.Close()
	} else {
		connErr(callerPackageName)
		return nil, errors.New("HTML body empty for: " + url)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		Logging.LogError(readErr.Error())
		connErr(callerPackageName)
		return nil, readErr
	}

	if connectionError[callerPackageName] {
		Logging.LogMessage("Connection found!")
		connectionError[callerPackageName] = false
	}
	return body, nil
}
